using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace TestAssignment.BackgroundWorker
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private readonly IBackgroundTaskQueue _queue;
        public Worker(ILogger<Worker> logger, IBackgroundTaskQueue queue)
        {
            _logger = logger;
            _queue = queue;
        }

        protected override async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Queued Hosted Service is starting.");
            while (!cancellationToken.IsCancellationRequested)
            {
                _logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);
                var workItem = await _queue.DequeueAsync(cancellationToken);

                try
                {
                    await workItem(cancellationToken);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex,
                       "Error occurred executing {WorkItem}. \n StackTrace: {stacktrace}", nameof(workItem), ex.StackTrace);
                }
            }
            _logger.LogInformation("Queued Hosted Service is stopping.");
        }
    }
}