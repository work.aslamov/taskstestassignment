﻿namespace TestAssignment
{
    public class TaskVM
    {
        public string Status { get; private set; } = "";
        public string TimeStamp { get; private set; } = "";

        public TaskVM(string status, string timeStamp)
        {
            Status = status;
            TimeStamp = timeStamp;
        }
    }
}
