﻿using TestAssignment.DataAccessLayer;
using TestAssignment.BackgroundWorker;

namespace TestAssignment
{
    public class TaskService: ITaskService
    {
        private readonly ILogger<TaskService> _logger;
        private readonly IBackgroundTaskQueue _backgroundWorkerQueue;
        private readonly IServiceScopeFactory _serviceScopeFactory;

        public TaskService(ILogger<TaskService> logger, IBackgroundTaskQueue backgroundWorkerQueue, IServiceScopeFactory serviceScopeFactory)
        {
            _logger = logger;
            _backgroundWorkerQueue = backgroundWorkerQueue;
            _serviceScopeFactory = serviceScopeFactory;
        }
        public async Task<Guid> CreateTaskAsync(ITaskRepository repository)
        {
            var guid = Guid.NewGuid();

            _logger.LogInformation($"Start creating new task at {DateTime.UtcNow}");

            var task = new TaskEntity(guid.ToString(), DateTime.UtcNow.ToString("o"), TaskStatuses.created.ToString());
            await repository.AddAsync(task);

            _logger.LogInformation($"Done at {DateTime.UtcNow.TimeOfDay}");

            return guid;
        }

        public async Task RunTaskAsync(Guid guid, ITaskRepository repository, int delayMillisecs)
        {
            _logger.LogInformation($"Start running new task at {DateTime.UtcNow}");

            _backgroundWorkerQueue.QueueBackgroundWorkItem(async token =>
            {
                using (var scope = _serviceScopeFactory.CreateScope())
                {
                    // get the new instance of repository, because previous has been disposed after response
                    var context = scope.ServiceProvider.GetRequiredService<ITaskRepository>();
                    await UpdateTaskStatusAsync(guid.ToString(), context, TaskStatuses.running);
                    await Task.Delay(delayMillisecs);
                    await UpdateTaskStatusAsync(guid.ToString(), context, TaskStatuses.finished);
                    _logger.LogInformation($"Finished running new task at {DateTime.UtcNow}");
                }
            });
        }

        public Task UpdateTaskStatusAsync(string guid, ITaskRepository repository, TaskStatuses status)
        {
             return repository.UpdateStatusAsync(guid, status.ToString());
        }

        public async Task<TaskEntity?> GetTaskByIdAsync(string guid, ITaskRepository repository)
        {
            return await repository.GetByGuidAsync(guid);
        }
    }
}
