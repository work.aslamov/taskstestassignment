﻿using TestAssignment.DataAccessLayer;

namespace TestAssignment
{
    public interface ITaskService
    {
        Task<Guid> CreateTaskAsync(ITaskRepository db);
        Task UpdateTaskStatusAsync(string guid, ITaskRepository db, TaskStatuses status);
        Task RunTaskAsync(Guid guid, ITaskRepository db, int delayMillisecs);
        Task<TaskEntity?> GetTaskByIdAsync(string guid, ITaskRepository repository);
    }
}
