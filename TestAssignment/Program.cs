using Microsoft.EntityFrameworkCore;
using TestAssignment;
using TestAssignment.DataAccessLayer;
using TestAssignment.BackgroundWorker;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var connection = "Data Source=testApp.db";
builder.Services.AddDbContext<ApplicationDbContext>(options =>
{
    options.UseSqlite(connection);
});

builder.Services.AddHostedService<Worker>();
builder.Services.AddSingleton<IBackgroundTaskQueue, BackgroundWorkerQueue>();
builder.Services.AddTransient<ITaskService, TaskService>();
builder.Services.AddScoped<ITaskRepository, TaskRepository>();

await using var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

var taskService = app.Services.GetService<ITaskService>();
var taskRepository = app.Services.GetService<TaskRepository>();

app.MapPost("/task", async (ITaskRepository taskRepository) =>
{
    var guid = await taskService!.CreateTaskAsync(taskRepository);
    // 120000 ms == 2 mins
    taskService.RunTaskAsync(guid, taskRepository, 120000);
    return Results.Accepted(null, guid);
});

app.MapGet("/task/{id}", async (string id, ITaskRepository taskRepository) =>
{
    if (!Guid.TryParse(id, out _))
        return Results.BadRequest();

    var task = await taskService!.GetTaskByIdAsync(id, taskRepository);

    if (task == null)
        return Results.NotFound();

    return Results.Json(new TaskVM(task.Status, task.TimeStamp));
});

await app.RunAsync();