﻿Solution for the test assignment 
========================================================

Solution contains 3 projects:

* `DataAccessLayer` class library for all the work related to the DB

* `TestAssignmentAPI` is a WebAPI with two endpoints.

There are two possible request:

`POST /task` without parameters does the following: it creates a new task, assign status `created` to it and returns it's GUID and 200 status. 
    After task is created 3 tasks are queued to background worker: 1. Change status to `running` 2. Wait for 2 minutes 3. Change status to `finished`
    and they are performed one after another. 

`GET /task/{id}` where id is GUID string. It Does the following: 

    1. Returns 400 status if id is not a valid GUID

    2. Returns 404 status if task with such GUID is not found in the db

    3. Returns JSON with content
    {
        "status": ":status" // :status == created, running or finished
        "timestamp": "the time when the status change occurred" // datetime in the ISO 8601 format
    }   and 200 status

* `BackgroundWorker` class library that performs long running background work. First some work is added to the queue and than worker dequeue tasks and performs them. 
