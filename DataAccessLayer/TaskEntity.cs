﻿namespace TestAssignment.DataAccessLayer
{
    public class TaskEntity
    {
        public string Guid { get; set; } = "";
        public string TimeStamp { get; set; } = "";
        public string Status { get; set; } = "";

        private TaskEntity() { }

        public TaskEntity(string guid, string timeStamp, string status)
        {
            Guid = guid;    
            TimeStamp = timeStamp;
            Status = status;
        }
    }
}
