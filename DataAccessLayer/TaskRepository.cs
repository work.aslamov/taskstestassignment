﻿using Microsoft.EntityFrameworkCore;

namespace TestAssignment.DataAccessLayer
{
    public class TaskRepository: ITaskRepository
    {
        private readonly ApplicationDbContext _dbContext;

        public TaskRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public Task AddAsync(TaskEntity task)
        {
            _dbContext.Tasks.AddAsync(task);
            return _dbContext.SaveChangesAsync();
        }

        public async Task<TaskEntity?> GetByGuidAsync(string guid)
        {
            return await _dbContext.Tasks.FirstOrDefaultAsync(t => t.Guid.Equals(guid));
        }

        public async Task UpdateStatusAsync(string guid, string status)
        {
            var result = await GetByGuidAsync(guid);
            if (result != null)
            {
                result.Status = status;
                result.TimeStamp = DateTime.UtcNow.ToString("o");
                await _dbContext.SaveChangesAsync();
            }
        }
    }
}
