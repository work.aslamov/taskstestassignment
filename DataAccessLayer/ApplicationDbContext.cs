﻿using Microsoft.EntityFrameworkCore;

namespace TestAssignment.DataAccessLayer
{
    public class ApplicationDbContext: DbContext
    {
        public DbSet<TaskEntity> Tasks { get; set; } = null!;
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options) 
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new TaskConfig());
        }
    }
}