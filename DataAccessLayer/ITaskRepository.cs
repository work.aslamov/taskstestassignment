﻿namespace TestAssignment.DataAccessLayer
{
    public interface ITaskRepository
    {
        Task<TaskEntity?> GetByGuidAsync(string guid);
        Task AddAsync(TaskEntity task);
        Task UpdateStatusAsync(string guid, string status);
    }
}
