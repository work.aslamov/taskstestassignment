﻿namespace TestAssignment
{
    public enum TaskStatuses
    {
        created,
        running,
        finished
    }
}
