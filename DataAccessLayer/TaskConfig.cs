﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace TestAssignment.DataAccessLayer
{
    public class TaskConfig : IEntityTypeConfiguration<TaskEntity>
    {
        public void Configure(EntityTypeBuilder<TaskEntity> entity)
        {
            entity.HasKey(e => e.Guid);
            entity.HasIndex(e => e.Guid).IsUnique();
            entity.Property(p => p.Guid).HasColumnType("Text");
            entity.Property(p => p.TimeStamp).HasColumnType("Text");
            entity.Property(p => p.Status).HasColumnType("Text");

            entity.HasData(new TaskEntity("a484edb9-da7b-4370-9ac1-be7076682454", "2022-05-11T13:57:31.2311892-04:00", TaskStatuses.finished.ToString()));
        }
    }
}
